/*
* File gom:
+ Tat ca cac cau truc du lieu dung cho phan mem\
+ Nhung ham dung chung cho cac doi tuong cua phan mem(doc gia, quan ly thu vien, adm he thong) gom Show_InFor, Change_PassWord, Update_Account
+ Nhung ham trung gian: getPassWord, Save_DB_Account
+ Nhung ham giao dien: Show....
*/
#ifndef Data_Strutures
#define Data_Structures
#include "Special_func.h"
//Thu vien he thong
#include <map>
#include <stdio.h>
#include <conio.h>
#include <sstream>

using namespace std;

struct Date
{
	int Day;
	int Month;
	int Year;
};

struct Infor_Account
{
	string UserName = "";
	string PassWord = "";
	string Id = "";
	string FullName = "";
	Date BirthDay;
	bool Status = true;
	int Role = 0;
	string Phone = "";
	string Mail = "";
};

struct Infor_User_Borrowed
{
	string Id_people = "";
	string Id_Book = "";
	string Name_Book = "";
	Date Date_From;
	Date Date_To;
	string Note = "";
};

struct Infor_Book
{
	string Id = "";
	string Name = "";
	int Amount = 0;
	long Price = 0;
	string Author = "";
	string Describe = "";

};
void GetTime_System(Date &temp);
void getPassWord(string &str);
void Show_Infor(); //Done
void Load_form_Infor(); //Done
void Load_form_changeInfor(); //Done
void Load_form_ChangePass(); //Done
void Load_form_ListBook(); //Done
int Register_Account(int); //Done
bool Change_PassWord(); // Done
bool Save_DB_Account(); //Done
bool Update_Account(); //Done
bool Load_DB_Account(string); //Done
bool Load_DB_Books(string); //Coding
void Store_DB(string, string); //Done
void Print_List_Book(); //Done
Date GetTime_After_x(Date , int); //Done but didn't check => maybe wrong
#endif // !Data_Strutures
