#include "Menu_Admin.h"


using namespace std;

extern map<string, Infor_Account> List_Infor_Account;
extern Infor_Account Account_Curren;

void Menu_Adm()
{
	string temp = "";
	string userName = "";
	int choose = 0;
	while (true)
	{
		system("cls");
		Load_form_ADM();
		cout << "\nCac chuc nang cho admin he thong:";
		cout << "\n1.Tao tai khoan cho manager"; //done
		cout << "\n2.Khoa tai khoan"; //done
		cout << "\n3.Mo khoa tai khoan"; //done
		cout << "\n4.Reset Password cho tai khoan khac"; // Done
		cout << "\n5.Xem thong tin ca nhan"; //done
		cout << "\n6.Thay doi thong tin ca nhan"; //done
		cout << "\n7.Thay doi mat khau"; // done
		cout << "\n8.Thoat ung dung"; // done
		cout << "\nMoi ban chon tinh nang:";
		cin >> choose;
		cin.ignore();
		if (choose == 1)
		{
			system("cls");
			cout << "TAO TAI KHOAN CHO MANAGER HE THONG\n";
			bool check = Register_Account_for_Manager();
			if (check)
			{
				cout << "Tao tai khoan thanh cong\n";
				cout << "Nhan Enter de quay lai menu truoc:";
				getline(cin, temp);
			}
		}
		else if (choose == 2)
		{
			system("cls");
			userName = "";
			bool check_Lock = false;
			cout << "Khoa tai khoan su dung username:\n";
			cout << "Nhap vao username tai khoan can khoa:";
			getline(cin, userName);
			check_Lock = Lock_Account(userName);
			if (check_Lock)
			{
				cout << "Khoa tai khoan " + userName << " thanh cong!";
				cout << "Nhan Enter de quay lai menu truoc:";
				getline(cin, temp);
			}
			else
			{
				cout << "Username vua nhap khong ton tai trong he thong\n";
				cout << "Nhan Enter de quay lai menu truoc";
				getline(cin, temp);
			}
		}
		else if (choose == 3)
		{
			system("cls");
			userName = "";
			bool check_Lock = false;
			cout << "Khoa tai khoan su dung username:\n";
			cout << "Nhap vao username tai khoan can mo khoa:";
			getline(cin, userName);
			check_Lock = UnLock_Account(userName);
			if (check_Lock)
			{
				cout << "Mo tai khoan " + userName << " thanh cong!";
				cout << "Nhan Enter de quay lai menu truoc:";
				getline(cin, temp);
			}
			else
			{
				cout << "Username vua nhap khong ton tai trong he thong\n";
				cout << "Nhan Enter de quay lai menu truoc";
				getline(cin, temp);
			}
		}
		else if (choose == 4)
		{
			system("cls");
			userName = "";
			bool check_Re = false;
			cout << "RESET PASSWORD su dung username:\n";
			cout << "Nhap vao username tai khoan can reset:";
			getline(cin, userName);
			check_Re = Reset_Password(userName);
			if (check_Re)
			{
				cout << "Reset_Password cho username: " + userName << " thanh cong\n";
				cout << "Nhan Enter de quay lai menu truoc:";
				getline(cin, temp);
			}
			else
			{
				cout << "Username vua nhap khong ton tai trong he thong\n";
				cout << "Nhan Enter de quay lai menu truoc";
				getline(cin, temp);
			}
		}
		else if (choose == 5)
		{
			Show_Infor();
		}
		else if (choose == 6)
		{
			bool check_update = false;
			check_update = Update_Account();
			system("cls");
			if (check_update)
			{
				cout << "Cap nhat thong tin tai khoan thanh cong\n";
				cout << "ENTER de quay lai man hinh chinh\n";
				getline(cin, temp);
			}
			else
			{
				cout << "Cap nhat thong tin tai khoan that bai\n";
				return;
			}
		}
		else if (choose == 7)
		{
			bool check = Change_PassWord();
			system("cls");
			if (check)
			{
				cout << "CAP NHAT MAT KHAU THANH CONG\n";
				cout << "Enter de quay lai man hinh chinh\n";
				getline(cin, temp);

			}
			else
			{
				cout << "CAP NHAT MAT KHAU THAT BAI\n";
				return;
			}
		}
		else if (choose == 8)
		{
			return;
		}
	}
}

void Load_form_ADM()
{
	string temp = "";
	Set_Color(4);
	ifstream Load_file("DataBase/LogoMenuAdm.txt", ios::in);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}
	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		cout << "\n" << temp;
	}
	Set_Color(7);
	Load_file.close();
}

bool Register_Account_for_Manager()
{
	int role = 1;
	int check = Register_Account(role);
	return check;
}

bool Reset_Password(string username)
{
	map<string, Infor_Account>::iterator it;
	Infor_Account temp;
	it = List_Infor_Account.find(username);
	if (it != List_Infor_Account.end())
	{
		it->second.PassWord = "0000";
		temp = it->second;
		List_Infor_Account[temp.UserName] = temp;
		//ghi lai du lieu tu List_Infor_Account moi xuong file Account
		bool check = Save_DB_Account();
		if (check == true)
		{
			return 1;
		}
		else
			return 0;
	}
	return 0;

}

bool Lock_Account(string username)
{
	map<string, Infor_Account>::iterator it;
	Infor_Account temp;
	it = List_Infor_Account.find(username);
	if (it != List_Infor_Account.end())
	{
		it->second.Status = 0;
		temp = it->second;
		List_Infor_Account[temp.UserName] = temp;
		//Ghi lai du lieu tu List_infor_Account xuong file Account.txt
		bool check = Save_DB_Account();
		if (check)
		{
			return 1;
		}
		else 
			return 0;
	}
	return 0;
}

bool UnLock_Account(string username)
{
	map<string, Infor_Account>::iterator it;
	Infor_Account temp;
	it = List_Infor_Account.find(username);
	if (it != List_Infor_Account.end())
	{
		it->second.Status = 1;
		temp = it->second;
		List_Infor_Account[temp.UserName] = temp;
		//Ghi lai du lieu tu List_infor_Account xuong file Account.txt
		bool check = Save_DB_Account();
		if (check)
		{
			return 1;
		}
		else
			return 0;
	}
	return 0;
}
