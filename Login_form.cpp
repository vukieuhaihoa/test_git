#include "Login_form.h"
#include "Menu_Student.h"
#include "Menu_Manager.h"
#include "Menu_Admin.h"

#pragma warning(disable:4996)

//Du Lieu Toan Cuc 

extern map<string, Infor_Account> List_Infor_Account;
extern Infor_Account Account_Curren;

using namespace std;

void Load_form()
{
	int choose = 0;
	Set_Color(9);
	ifstream Logo("DataBase/Logo.txt", ios::in);
	if (!Logo)
	{
		cout << "FILE NOT FOUND\n";
		return;
	}
	while (!Logo.eof())
	{
		string str_temp = "\0";
		getline(Logo, str_temp);
		cout << "\n";
		cout << str_temp;
	}
	Logo.close();
	Set_Color(7);
	cout << "Moi ban chon tinh nang:\n";
	cout << "1.Login\n";
	cout << "2.Dang ky tai khoan\n";
	cout << "Moi ban nhap: ";
	cin >> choose;
	if (choose == 1)
	{
		Menu_Login();
		return;
	}
	if (choose == 2)
	{
		int role = 0;
		cin.ignore();
		system("cls");
		Register_Account(role);
		system("cls");
		cout << "Chuc mung ban da dang ki tai khoan thanh cong!\n";
		cout << "Moi ban khoi dong lai ung dung de truy cap vao phan mem bang tai khoan moi\n";
	}
}

int Load_Login_form()
{
	system("cls");
	Set_Color(3);
	ifstream Login_form("DataBase/LogoLogin.txt", ios::in);
	if (!Login_form)
	{
		cout << "FILE NOT FOUND\n";
		return 0;
	}
	string str = "";
	while (!Login_form.eof())
	{
		str = "";
		getline(Login_form, str);
		cout << "\n";
		cout << str;
	}
	Login_form.close();
	Set_Color(7);
	cin.ignore();
	return 1;
}

void Menu_Login()
{
	int checkfile = Load_Login_form();
	if (checkfile == 0)
	{
		return;
	}
	while (true)
	{
		//Xoa man hinh va dua con tro ve vi tri cu
		gotoxy(0, 12); 
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                       \n";
		cout << "                                                      ";
		gotoxy(0, 12);

		// Get passwork and username from user
		string username = "";
		string password = "";
		cout << "Ten dang nhap:";
		getline(cin, username);
		cout << "Mat khau     :";
		getPassWord(password);
		// check account in database
		cout << "\n";
		bool check = Login(username, password);
		if (check == 0)
		{
			int choose;
			cout << "Sai ten dang nhap or mat khau!\nChon 1 de quay lai dang nhap\nChon 2 de thuc hien chuc nang quen mat khau\n";
			cout << "Moi ban nhap: ";
			cin >> choose;
			//fflush(stdin);
			cin.ignore();
			if (choose == 2)
				//Chua thuc hien tinh nang quen mat khau
				return;
		}
		else
		{
			if (Account_Curren.Status == 0)
			{
				system("cls");
				cout << "TAI KHOAN CUA BAN HIEN DANG BI KHOA LIEN HE TRUC TIEP VOI QUAN LY THU VIEN DE DUOC MO\n";
				return;
			}
			else
			{
				//Load menu cho sinh vien
				if (Account_Curren.Role == 0)
				{
					int x = wherex();
					int y = wherey();
					//cout << "vi tri con tro hien tai " << x << "__" << y << endl;
					Menu_Student();
				}
				else if (Account_Curren.Role == 1) //Load menu cho quan ly thu vien
				{
					Menu_Manager();
				}
				else if (Account_Curren.Role == 2) // Load menu cho adm system
				{
					Menu_Adm();
				}
			}
			
			//cout << "Dang nhap thanh cong!!!\n";
			return;
		}
	}
}

bool Login(string username, string password)
{
	
	if (username == "" || password == "")
	{
		return 0;
	}
	/*for (map<string, Infor_Account>::iterator i = List_Infor_Account.begin(); i != List_Infor_Account.end(); i++)
	{
		if (username.compare(i->first) == 0)
		{
			if (password.compare(i->second.PassWord) == 0)
			{
				Account_Curren = i->second;
				return 1;
			}
		}
	}*/
	map<string, Infor_Account>::iterator it;
	it = List_Infor_Account.find(username);
	// ham find neu tim thay no se tro toi vi tri do, neu khong no se tro den vi tri end map;
	if (it == List_Infor_Account.end())
	{
		return 0;
	}
	if (username.compare(it->first) == 0)
	{
		if (password.compare(it->second.PassWord) == 0)
		{
			Account_Curren = it->second;
			return 1;
		}
	}
	return 0;
}


