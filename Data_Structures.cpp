#include "Data_Structures.h"
#include <ctime>
#include <iomanip>
#pragma warning(disable:4996)

using namespace std;

extern map<string, Infor_Account> List_Infor_Account;
extern Infor_Account Account_Curren;
extern map<string, Infor_Book> List_Infor_Books;

ostream& operator << (ostream &output, Date dd)
{
	string temp_day = "";
	string temp_month = "";
	string temp_year = "";
	if (dd.Day < 10)
	{
		temp_day += "0" + to_string(dd.Day);
	}
	else
	{
		temp_day += to_string(dd.Day);
	}
	if (dd.Month < 10)
	{
		temp_month += "0" + to_string(dd.Month);
	}
	else
	{
		temp_month += to_string(dd.Month);
	}
	temp_year = to_string(dd.Year);
	output << temp_day + "/" + temp_month + "/" + temp_year << "\n";
	return output;
}

void GetTime_System(Date &temp)
{
	std::time_t t = std::time(0);   // get time now
	std::tm* now = std::localtime(&t);
	temp.Year = now->tm_year + 1900;
	temp.Month = now->tm_mon + 1;
	temp.Day = now->tm_mday;
}

void getPassWord(string &pass)
{
	char temp[50];
	int i = 0;
	while (int(temp[i] = getch()) != 13 && i < 48)
	{
		if (i == 0 && int(temp[i]) == 8)
		{
			continue;
		}
		if (int(temp[i]) == 8 && i > 0)
		{
			temp[i] = NULL;
			cout << "\b";
			cout << " ";
			cout << "\b";
			i--;
		}
		else
		{
			cout << "*";
			i++;
		}
	}
	temp[i] = '\0';
	pass = string(temp);
}

void Show_Infor()
{
	system("cls");
	Load_form_Infor();
	//cout << "------THONG TIN TAI KHOAN------\n";
	cout << "\nId:" << Account_Curren.Id << endl;
	cout << "\nFullName:" << Account_Curren.FullName << endl;
	cout << "\nBirthDay:" << Account_Curren.BirthDay;
	cout << "\nMail:" << Account_Curren.Mail << endl;
	cout << "\nPhone:" << Account_Curren.Phone << endl;
	cout << "\nNhan enter de tro lai menu chinh";
	string temp;
	getline(cin, temp);
}

void Load_form_Infor()
{
	ifstream Load_file("DataBase/LogoInfor.txt", ios::in);
	Set_Color(3);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}
	string temp = "";
	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		cout << "\n";
		cout << temp;
	}
	Load_file.close();
	Set_Color(7);
}

void Load_form_changeInfor()
{
	ifstream Load_file("DataBase/LogoChangeInfor.txt", ios::in);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}
	string temp = "";
	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		cout << "\n";
		cout << temp;
	}
	Load_file.close();
}

void Load_form_ChangePass()
{
	string temp = "";
	ifstream Load_file("DataBase/LogoChangePass.txt", ios::in);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}

	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		cout << temp << "\n";
	}
	return;
}

void Load_form_ListBook()
{
	string temp = "";
	Set_Color(13);
	ifstream Load_file("DataBase/LogoListBooks.txt", ios::in);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}

	while (!Load_file.eof())
	{
		temp = "";
		getline(Load_file, temp);
		cout << temp << "\n";
	}
	return;
}

int Register_Account(int role)
{
	Infor_Account newMember;
	Date newDate;
	string temp = "";
	string temp_day = "";
	string temp_month = "";
	string temp_year = "";
	string check_match_usrname = "";
	bool check_exist = true;
	map<string, Infor_Account>::iterator it;
	do
	{
		system("cls");
		cout << "MOI BAN DIEN DAY DU NHUNG THONG TIN SAU DAY:\n";
		cout << "Username:";
		getline(cin, newMember.UserName);
		it = List_Infor_Account.find(newMember.UserName);
		if (it != List_Infor_Account.end())
		{
			cout << "Username Da ton tai trong he thong!, Moi ban nhap lai ^^";
			getline(cin, check_match_usrname);
		}
		else
			check_exist = false;
	} while (check_exist);
	cout << "\nPassword:";
	getPassWord(newMember.PassWord);
	cout << "\n";
	cout << "\nId:";
	getline(cin, newMember.Id);
	cout << "\nHo va ten:";
	getline(cin, newMember.FullName);
	cout << "\nPhone:";
	getline(cin, newMember.Phone);
	cout << "\nMail:";
	getline(cin, newMember.Mail);
	cout << "\nBirthDay:\n" << "+ Date:";
	cin >> newDate.Day;
	cin.ignore();
	cout << "\n+ Month:";
	cin >> newDate.Month;
	cin.ignore();
	cout << "\n+ Year:";
	cin >> newDate.Year;
	cin.ignore();
	newMember.Status = 1;
	newMember.Role = role;
	List_Infor_Account[newMember.UserName] = newMember;
	// ghi du lieu xuong Account

	if (newDate.Day < 10)
	{
		temp_day = "0" + to_string(newDate.Day);
	}
	else
	{
		temp_day += to_string(newDate.Day);
	}

	if (newDate.Month < 10)
	{
		temp_month = "0" + to_string(newDate.Month);
	}
	else
	{
		temp_month += to_string(newDate.Month);
	}

	temp_year += to_string(newDate.Year);
	temp = "\n" + newMember.UserName + "|" + newMember.PassWord + "|" + newMember.Id + "|" + newMember.FullName + "|" + temp_day + "/" + temp_month + "/" + temp_year + "|" + to_string(newMember.Status) + "|" + to_string(newMember.Role) + "|" + newMember.Phone + "|" + newMember.Mail + "|";
	Store_DB("DataBase/Account.txt", temp);
	return 1;
}

bool Update_Account()
{
	Infor_Account newChange;
	Date newDate;
	string temp = "";
	string temp_day = "";
	string temp_month = "";
	string temp_year = "";
	system("cls");
	Set_Color(6);
	Load_form_changeInfor();
	Set_Color(7);
	cout << "\nHo va ten:";
	getline(cin, newChange.FullName);
	cout << "\nPhone:";
	getline(cin, newChange.Phone);
	cout << "\nMail:";
	getline(cin, newChange.Mail);
	cout << "\nBirthDay:\n" << "+ Date:";
	cin >> newChange.BirthDay.Day;
	cin.ignore();
	cout << "\n+ Month:";
	cin >> newChange.BirthDay.Month;
	cin.ignore();
	cout << "\n+ Year:";
	cin >> newChange.BirthDay.Year;
	cin.ignore();
	//NHUNG THONG TIN KHONG DUOC PHEP THAY DOI LA
	newChange.Id = Account_Curren.Id;
	newChange.Status = Account_Curren.Status;
	newChange.Role = Account_Curren.Role;
	newChange.UserName = Account_Curren.UserName;
	newChange.PassWord = Account_Curren.PassWord;
	cout << "\nBan chac muon thay doi tai khoan chu?[y|n]:";
	char choose;
	cin >> choose;
	if (choose == 'y')
	{
		Account_Curren.FullName = newChange.FullName;
		Account_Curren.Phone = newChange.Phone;
		Account_Curren.Mail = newChange.Mail;
		Account_Curren.BirthDay.Day = newChange.BirthDay.Day;
		Account_Curren.BirthDay.Month = newChange.BirthDay.Month;
		Account_Curren.BirthDay.Year = newChange.BirthDay.Year;
		List_Infor_Account[Account_Curren.UserName] = Account_Curren;
		//Xoa du lieu file Account cu va ghi lai du lieu tu List_Infor_Account moi xuong Account
		bool check = Save_DB_Account();
		if (check == true)
		{
			return 1;
		}
		else
			return 0;
	}
	return 1;
}

bool Save_DB_Account()
{
	string temp = "";
	string temp_day = "";
	string temp_month = "";
	string temp_year = "";
	ofstream Load_file("DataBase/Account.txt", ios::trunc);
	Load_file.close();
	Load_file.open("DataBase/Account.txt", ios::out | ios::app);
	remove("DataBase/Account.txt");
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		return 0;
	}
	bool check = false;
	for (auto &x : List_Infor_Account)
	{
		if (x.second.BirthDay.Day < 10)
		{
			temp_day = "0" + to_string(x.second.BirthDay.Day);
		}
		else
		{
			temp_day = to_string(x.second.BirthDay.Day);
		}
		if (x.second.BirthDay.Month < 10)
		{
			temp_month = "0" + to_string(x.second.BirthDay.Month);
		}
		else
		{
			temp_month = to_string(x.second.BirthDay.Month);
		}
		temp_year = to_string(x.second.BirthDay.Year);
		temp = "";
		if (check == false)
		{
			temp = x.first + "|" + x.second.PassWord + "|" + x.second.Id + "|" + x.second.FullName + "|" + temp_day + "/" + temp_month + "/" + temp_year + "|" + to_string(x.second.Status) + "|" + to_string(x.second.Role) + "|" + x.second.Phone + "|" + x.second.Mail + "|";
			check = true;
		}
		else
			temp = "\n" + x.first + "|" + x.second.PassWord + "|" + x.second.Id + "|" + x.second.FullName + "|" + temp_day + "/" + temp_month + "/" + temp_year + "|" + to_string(x.second.Status) + "|" + to_string(x.second.Role) + "|" + x.second.Phone + "|" + x.second.Mail + "|";
		Load_file << temp;
	}
	Load_file.close();
	return 1;
}

bool Change_PassWord()
{
	system("cls");
	Set_Color(10);
	Load_form_ChangePass();
	Set_Color(7);
	string CurrentPass = "";
	string NewPass = "";
	string reNewPass = "";
	string temp = "";
	int x = wherex();
	int y = wherey();
	do
	{
		gotoxy(x, y);
		cout << "                                                                                         \n";
		cout << "                                                                                         \n";
		gotoxy(x, y);
		cout << "Moi ban nhap lai mat khau hien tai:";
		getPassWord(CurrentPass);
		if (Account_Curren.PassWord.compare(CurrentPass))
		{
			cout << "\nPassWord vua roi khong trung khop voi PassWord hien tai, moi ban enter de nhap lai!!!";
			getline(cin, temp);
		}
		else
		{
			break;
		}

	} while (true);
	cout << "\n";
	x = wherex();
	y = wherey();
	do
	{
		gotoxy(x, y);
		cout << "                                                                                         \n";
		cout << "                                                                                         \n";
		cout << "                                                                                         \n";
		gotoxy(x, y);
		cout << "Mat Khau moi:";
		getPassWord(NewPass);
		cout << "\nNhap lai mat khau moi:";
		getPassWord(reNewPass);
		if (NewPass.compare(reNewPass))
		{
			cout << "\nkhong trung khop,enter de nhap lai";
			getline(cin, temp);
		}
		else
		{
			break;
		}
	} while (true);
	Account_Curren.PassWord = NewPass;
	List_Infor_Account[Account_Curren.UserName] = Account_Curren;
	//Xoa du lieu file Account cu va ghi lai du lieu tu List_Infor_Account moi xuong Account
	bool check = Save_DB_Account();
	if (check == true)
	{
		return 1;
	}
	else
		return 0;
	return 1;
}

bool Load_DB_Account(string path)
{
	ifstream Account_file(path, ios::in);
	if (!Account_file)
	{
		cout << "FILE NOT FOUND\n";
		return 0;
	}
	Infor_Account Clone_Acc;
	string str_temp = "";
	string s[10];
	int i = 0; int j = 0;
	while (!Account_file.eof())
	{
		str_temp = "";
		getline(Account_file, str_temp);
		istringstream temp(str_temp);
		i = 0;
		while (getline(temp, s[i++], '|')){}
		////username|password|maso|ten|ngaysinh|status|role|sdt|mail
		Clone_Acc.UserName = s[0];
		Clone_Acc.PassWord = s[1];
		Clone_Acc.Id = s[2];
		Clone_Acc.FullName = s[3];
		// day so vao birthday
		istringstream temp1(s[4]);
		j = 0;
		string ss[4];
		while (getline(temp1, ss[j++], '/')){}
		Clone_Acc.BirthDay.Day = stoi(ss[0]);
		Clone_Acc.BirthDay.Month = stoi(ss[1]);
		Clone_Acc.BirthDay.Year = stoi(ss[2]);
		Clone_Acc.Status = stoi(s[5]);
		Clone_Acc.Role = stoi(s[6]);
		Clone_Acc.Phone = s[7];
		Clone_Acc.Mail = s[8];
		List_Infor_Account[s[0]] = Clone_Acc;
	}
	Account_file.close();
	return 1;
}

void Store_DB(string path, string content)
{
	ofstream Load_DB(path, ios::app | ios::out);
	Load_DB << content;
	Load_DB.close();
}

bool Load_DB_Books(string path)
{
	ifstream List_Books(path, ios::in);
	if (!List_Books)
	{
		cout << "FILE NOT FOUND\n";
		return false;
	}
	Infor_Book temp_Book;
	string temp = "";
	string str[10];
	int i = 0;
	while (!List_Books.eof())
	{
		temp = "";
		i = 0;
		getline(List_Books, temp);
		istringstream temp_get(temp);
		while (getline(temp_get, str[i++], '|')){}
		temp_Book.Id = str[0];
		temp_Book.Name = str[1];
		temp_Book.Amount = stoi(str[2]);
		temp_Book.Price = stol(str[3]);
		temp_Book.Author = str[4];
		temp_Book.Describe = str[5];
		List_Infor_Books[str[1]] = temp_Book;
	}
	List_Books.close();
	return true;
}

void Print_List_Book()
{
	cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
	cout << "|   ID   |            Name            | Price | AMT |      Author      |                   Describes                 |\n";
	cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
	cout << fixed << setfill(' ');
	for (auto& x : List_Infor_Books)
	{
		cout << "|" << setw(8) << x.second.Id << "|" << setw(28) << x.second.Name << "|" << setw(7) << setfill(' ') << x.second.Price << "|" << setw(5) << x.second.Amount << "|" << setw(18) << x.second.Author << "|" << setw(45) << x.second.Describe << "|\n";
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
	}
}

Date GetTime_After_x(Date current, int x)
{
	Date temp;
	bool check_nhuan = false;
	if (((current.Year % 4 == 0) && (current.Year % 100 != 0)) || current.Year % 400 == 0)
	{
		check_nhuan = true;
	}
	if (current.Month == 1 || current.Month == 3 || current.Month == 5 || current.Month == 7 || current.Month == 8 || current.Month == 10 || current.Month == 12)
	{
		if (current.Day + x > 31)
		{
			if (current.Month == 12)
			{
				temp.Day = current.Day + x - 31;
				temp.Month = 1;
				temp.Year = current.Year + 1;
			}
			else
			{
				temp.Day = current.Day + x - 31;
				temp.Month = current.Month + 1;
				temp.Year = current.Year;
			}
		}
		else
		{
			temp.Day = current.Day + x;
			temp.Month = current.Month;
			temp.Year = current.Year;
		}
	}
	else if (current.Month == 4 || current.Month == 6 || current.Month == 9 || current.Month == 11)
	{
		if (current.Day + x > 30)
		{
			temp.Day = current.Day + x - 30;
			temp.Month = current.Month + 1;
			temp.Year = current.Year;
		}
		else
		{
			temp.Day = current.Day + x;
			temp.Month = current.Month;
			temp.Year = current.Year;
		}
	}
	else // thang 2
	{
		if (check_nhuan)
		{
			if (current.Day + x > 29)
			{
				temp.Day = current.Day + x - 29;
				temp.Month = current.Month + 1;
				temp.Year = current.Year;
			}
			else
			{
				temp.Day = current.Day + x;
				temp.Month = current.Month;
				temp.Year = current.Year;
			}
		}
		else
		{
			if (current.Day + x > 28)
			{
				temp.Day = current.Day + x - 28;
				temp.Month = current.Month + 1;
				temp.Year = current.Year;
			}
			else
			{
				temp.Day = current.Day + x;
				temp.Month = current.Month;
				temp.Year = current.Year;
			}
		}
	}
	return temp;
}

