﻿- Sách: mã số, tên,số lượng(hiện tại), giá, tác giả, nội dung giới thiệu

- Danh sách các sách đã cho mượn:mã số(mssv) người mượn, mã sách,tên sách,
ngày mượn(set ngày từ lúc được duyệt),
ngày hết hạn(từ ngày duyệt + 10), ghi chú(nếu mất or mượn quá hạn).

- Thông báo của tài khoản sinh viên: Mã số, thông báo(thông báo từ thư viện và thông
báo hết hạn cho mượn sách)

- Danh sách những thông báo từ nhân viên: nội dụng thông báo, thời gian, tên nv đăng.

- Các đối tượng tương tác vào hệ thống: Admin, manager, student có
property:username, password, mã số, Tên, ngày sinh,status(0 là khóa, 1 là mở)
chức năng(0 là sinh viên,1 là nhân viên thư viện, 2 là adm hệ thống), sđt, mail.

các chức năng chung cho cả 3 là: 
+ Chỉnh sửa infor cá nhân(Id, username,role,status khong duoc thay doi) và thay đổi mật khẩu
+ đăng kí tài khoản
+ //không phải tính năng Sinh viên quên mật khẩu, mở khóa tk thì mail cho admin kèm theo user để được xử lý
các chức năng chuyên biệt.
chức năng của sinh viên:
+ xem bộ sách có trong thư viện
+ tìm sách theo tên
+ // không phải tính năng (muốn mượn sách thì phải tìm coi trong thư viện có cuốn đó không
nếu còn thì tự vào thư viện lấy sau đó ra quầy quản lý để được làm thủ tục mượn)
+ nhận thông báo về sắp đến hạn trả sách, trả tiền làm mất sách, mượn quá hạn,
nhận thông báo của nhân viên thư viện.

chức năng của nhân viên thư viện:
+ xem bộ sách có trong thư viện
+ duyệt cho sinh viên mượn sách
+ thêm xóa sửa ds Sách.
+ đăng, sửa(có sách mới chẳng hạn), xóa thông báo đã cũ;


chức năng của ADM:
+ reset password, khóa và mở khóa tài khoản.
+ tạo tài khoản cho nhân viên thư viện.
+ Nhận yêu cầu mở tài khoản.