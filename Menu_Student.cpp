#include "Menu_Student.h"
#include <iomanip>
#include <vector>
#pragma warning(disable:4996)

extern map<string, Infor_Account> List_Infor_Account;
extern Infor_Account Account_Curren;
extern map<string, Infor_Book> List_Infor_Books;
extern Infor_User_Borrowed List_Book_Borrowed[8];

using namespace std;

void Menu_Student()
{
	int choose;
	string temp = "";
	while (true)
	{
		system("cls");
		Load_form_student();
		cout << "Cac chuc nang danh cho sinh vien:\n";
		cout << "1.Truy cap kho sach cua thu vien\n"; //Codingcou
		cout << "2.Danh sach nhung sach da muon\n"; //Coding
		cout << "3.Thong bao\n"; //Coding
		cout << "4.Xem thong tin ca nhan\n"; //Done
		cout << "5.Thay doi thong tin ca nhan\n"; // Done
		cout << "6.Thay doi mat khau\n"; // Done
		cout << "7.Thoat ung dung\n"; // Done
		cout << "Moi ban chon chuc nang:";
		cin >> choose;
		if (choose == 1)
		{
			cin.ignore();
			while (true)
			{
				system("cls");
				Load_form_ListBook();
				Set_Color(7);
				cout << "Moi ban chon cac tinh nang sau:\n";
				cout << "1.Tai toan bo danh sach cac sach co trong thu vien\n"; //Done
				cout << "2.Tim sach theo ten\n";// chi tim dung nhu ten (ke ca hoa thuong) => se fix sau
				cout << "3.Quay lai menu ban dau\n"; //Done
				cout << "Moi ban nhap:";
				int choo = 0;
				cin >> choo;
				cin.ignore();
				if (choo == 1)
				{
					system("cls");
					Print_List_Book();
					cout << "ENTER de quay lai Menu truoc!";
					getline(cin, temp);
				}
				else if (choo == 2)
				{
					system("cls");
					cout << "Searching...\n";
					cout << "Moi ban nhap vao ten hoac mot phan ten cua sach can tim:";
					string Name_of_Book = "";
					getline(cin, Name_of_Book);
					bool check_exist = Search_Book(Name_of_Book);
					if (check_exist)
					{
						cout << "ENTER de quay lai Menu truoc!";
						getline(cin, temp);
					}
					else
					{
						cout << "Noi dung khong tim thay :(\n";
						cout << "ENTER de quay lai Menu truoc!";
						getline(cin, temp);
					}

				}
				else if (choo == 3)
				{
					break;
				}
			}
		}
		else if (choose == 2)
		{
			
		}
		else if (choose == 3)
		{
			
		}
		else if (choose == 4)
		{
			cin.ignore();
			Show_Infor();
		}
		else if (choose == 5)
		{
			cin.ignore();
			bool check = Update_Account();
			system("cls");
			if (check)
			{
				cout << "CAP NHAT THONG TIN THANH CONG\n";
				cout << "Enter de quay lai man hinh chinh\n";
				getline(cin, temp);

			}
			else
			{
				cout << "CAP NHAT THONG TIN THAT BAI\n";
				return;
			}
		}
		else if (choose == 6)
		{
			cin.ignore();
			bool check = Change_PassWord();
			system("cls");
			if (check)
			{
				cout << "CAP NHAT MAT KHAU THANH CONG\n";
				cout << "Enter de quay lai man hinh chinh\n";
				getline(cin, temp);

			}
			else
			{
				cout << "CAP NHAT MAT KHAU THAT BAI\n";
				return;
			}
		}
		else if(choose == 7)
		{
			return;
		}
	}
	
}

void Load_form_student()
{
	ifstream Load_file("DataBase/LogoMenuStudent.txt", ios::in);
	Set_Color(12);
	if (!Load_file)
	{
		cout << "FILE NOT FOUND\n";
		Load_file.close();
		return;
	}
	string str_temp = "";
	while (!Load_file.eof())
	{
		str_temp = "";
		getline(Load_file, str_temp);
		cout << "\n";
		cout << str_temp;
	}
	Load_file.close();
	Set_Color(7);
}

bool Search_Book(string keyword)
{
	map<string, Infor_Book> result;
	bool check = false;
	int position_first = -1;
	for (auto& x: List_Infor_Books)
	{
		position_first = x.first.find(keyword);
		if (position_first != -1)
		{
			check = true;
			result[x.first] = x.second;
		}
	}
	if (check == true)
	{
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		cout << "|   ID   |            Name            | Price | AMT |      Author      |                   Describes                 |\n";
		cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		cout << fixed << setfill(' ');
		for (auto& x : result)
		{
			cout << "|" << setw(8) << x.second.Id << "|" << setw(28) << x.second.Name << "|" << setw(7) << setfill(' ') << x.second.Price << "|" << setw(5) << x.second.Amount << "|" << setw(18) << x.second.Author << "|" << setw(45) << x.second.Describe << "|\n";
			cout << "+--------------------------------------------------------------------------------------------------------------------+\n";
		}
	}
	return check;
}

bool Load_List_of_Borrowed_Books(int &x) // x la so cuon sach da muon
{
	string temp = "";
	string str[6];
	int i = 0;
	int j = 0;
	int count = 0;
	string ss[4];

	// moi file load du lieu len
	ifstream Load_file("DataBase/Borrowed_Book.txt", ios::in);
	if (!Load_file)
	{
		return false;
	}
	while (!Load_file.eof())
	{
		Infor_User_Borrowed clone;
		temp = "";
		getline(Load_file, temp);
		istringstream istr(temp);
		i = 0;
		while (getline(istr, str[i++], '|')){}
		
		clone.Id_people = str[0];
		clone.Id_Book = str[1];
		clone.Name_Book = str[2];
		
		istringstream temp1(str[3]);
		j = 0;
		while (getline(temp1, ss[j++], '/')){}
		clone.Date_From.Day = stoi(ss[0]);
		clone.Date_From.Month = stoi(ss[1]);
		clone.Date_From.Year = stoi(ss[2]);
		
		istringstream temp2(str[4]);
		j = 0;
		while (getline(temp1, ss[j++], '/')){}
		clone.Date_To.Day = stoi(ss[0]);
		clone.Date_To.Month = stoi(ss[1]);
		clone.Date_To.Year = stoi(ss[2]);

		clone.Note = str[5];
		List_Book_Borrowed[count++] = clone;
	}
	x = count - 1;
	Load_file.close();
	return true;
}